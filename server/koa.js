/**
 * Koa config
 */

'use strict';

let logger = require('./logger');
let util = require('util');
let config = require('./config');
let session = require('koa-session');

module.exports = function (app) {

  app.keys = [config.get('session_key')];
  app.use(session(app));

  app.use(require('koa-body')());
  app.use(require('koa-validate')());

  // Logger
  app.use(function *(next) {
    let start = Date.now();
    this.logger = logger.child({req: this.request});
    yield next;
    let ms = Date.now() - start;
    this.set('X-Response-Time', ms + 'ms');
    this.logger.debug(util.format('%s %s - %s', this.method, this.url, ms), {time: ms});
  });

  // Error handler
  app.use(function *(next) {
    try {
      yield next;
    } catch (err) {
      this.status = err.status || 500;
      this.body = {error: err.message};
      if (this.status === 500) {
        this.app.emit('error', err, this);
      }
    }
  });
  app.on('error', function (err, ctx) {
    ctx.logger.error(err, 'server error');
  });

  //Validator
  app.use(function *(next) {
    this.checkErrors = function(){
      if (this.errors){
        throw {status: 400, message: this.errors};
      }
    }.bind(this);
    yield next;
  });

};
