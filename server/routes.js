/**
 * Main application routes
 */

'use strict';

var mount = require('koa-mount');

module.exports = function(app) {

  // YEOMAN INJECT ROUTES BELOW
  app.use(mount('/api/v1/product', require('./resources/product')));
  app.use(mount('/api/v1/user', require('./resources/user')));
  app.use(mount('/', require('./resources/root')));

};
