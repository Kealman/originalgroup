/**
 * Main application file
 */

'use strict';

// Bootstrap server
var app = require('koa')();
require('./koa')(app);
require('./routes')(app);

// Expose app
exports = module.exports = app;
