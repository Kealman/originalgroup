'use strict';

let nameProperty = Symbol('name'); // User name
let bonusProperty = Symbol('bonus'); // Current bonus available
let bonusMaxProperty = Symbol('bonusMax'); // Max bonus available
let bonusPercentProperty = Symbol('bonusPercent'); // Compensated percent
let productsProperty = Symbol('products'); // Products list

let calculatePriceWithBonusMethod = Symbol('calculatePriceWithBonus');

let calculateCompensatedBonus = function (price, bonusPercent) {
  return price * (bonusPercent / 100);
};

let calculatePriceWithBonus = function (product) {
  let compensated = calculateCompensatedBonus(product.price, this.private[bonusPercentProperty]);

  let priceWithBonus;
  if (this.private[bonusProperty] < compensated) {
    priceWithBonus = product.price - this.private[bonusProperty];
    this.private[bonusProperty] = 0;
  } else {
    priceWithBonus = product.price - compensated;
    this.private[bonusProperty] -= compensated;
  }

  return priceWithBonus;
};

class User {

  constructor(name, bonus, bonusPercent) {
    this.private = {};
    this.private[nameProperty] = name;
    this.private[bonusProperty] = bonus;
    this.private[bonusMaxProperty] = bonus;
    this.private[bonusPercentProperty] = bonusPercent;
    this.private[productsProperty] = [];

    this.private[calculatePriceWithBonusMethod] = calculatePriceWithBonus.bind(this);
  }

  getProfile() {
    return {
      name: this.private[nameProperty],
      bonus: this.private[bonusProperty],
      bonusPercent: this.private[bonusPercentProperty],
      products: this.private[productsProperty]
    };
  }

  setProduct(product) {
    let priceWithBonus = this.private[calculatePriceWithBonusMethod](product);

    this.private[productsProperty].push({
      id: product.id,
      name: product.name,
      price: product.price,
      priceWithBonus: priceWithBonus
    });

    return {
      productData: this.private[productsProperty],
      currentAvailableBonus: this.private[bonusProperty]
    };
  }

  updateProduct(product) {
    this.private[bonusProperty] = this.private[bonusMaxProperty];
    this.private[productsProperty] = this.private[productsProperty].map(function (val) {
      if (val.id === product.id) {
        val.name = product.name;
        val.price = product.price;
      }

      val.priceWithBonus = this.private[calculatePriceWithBonusMethod](val);
      return val;
    }.bind(this));
    return {
      productData: this.private[productsProperty],
      currentAvailableBonus: this.private[bonusProperty]
    };
  }

  deleteProduct(id) {
    this.private[bonusProperty] = this.private[bonusMaxProperty];
    let delIndex = null;
    this.private[productsProperty].forEach(function (val, key) {
      if (val.id === id) {
        delIndex = key;
      } else {
        val.priceWithBonus = this.private[calculatePriceWithBonusMethod](val);
        return val;
      }
    }.bind(this));

    if (delIndex !== null) {
      this.private[productsProperty].splice(delIndex, 1);
    }

    return {
      productData: this.private[productsProperty],
      currentAvailableBonus: this.private[bonusProperty]
    };
  }

}


module.exports = User;
