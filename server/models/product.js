'use strict';

let inc = 0;

class Product {

  constructor(name, price) {
    this.id = ++inc;
    this.name = name;
    this.price = price;
  }

}

module.exports = Product;
