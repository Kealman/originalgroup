'use strict';

let controller = require('./product.controller');
let router = require('koa-router')();

router.post('/', controller.create);
router.put('/:id', controller.update);
router.del('/:id', controller.remove);

module.exports = router.routes();
