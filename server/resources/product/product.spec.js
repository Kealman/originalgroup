'use strict';


var app = require('../../server');
var request = require('supertest').agent(app.listen());

var expect = require('chai').expect;


describe('GET /api/v1/product', function(){
  it('should respond with 200 type Array', function(done){
    request
    .get('/api/v1/product')
    .expect(200, function(err, res) {
    	expect(res.body).deep.eql({hello: 'world'});
    	done();
    });
  });
});
