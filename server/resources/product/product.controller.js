'use strict';

let Product = require('../../models/product');
let Storage = require('../../storage');

exports.create = function* (next) {
  if (!this.session.user) {
    this.status = 401;
    return;
  }
  checkProductData(this);
  let product = new Product(this.request.body.name, this.request.body.price);
  this.body = Storage.getUser(this.session.user).setProduct(product);
};

exports.update = function* (next) {
  if (!this.session.user) {
    this.status = 401;
    return;
  }

  checkProductWithId(this);

  this.body = Storage.getUser(this.session.user).updateProduct({
    id: Number(this.params.id),
    name: this.request.body.name,
    price: Number(this.request.body.price)
  });
};

exports.remove = function* (next) {
  if (!this.session.user) {
    this.status = 401;
    return;
  }
  checkProductId(this);

  this.body = Storage.getUser(this.session.user).deleteProduct(Number(this.params.id));
};


function checkProductId(ctx) {
  ctx.checkParams('id').isInt();
  ctx.checkErrors();
}

function checkProductWithId(ctx) {
  ctx.checkParams('id').isInt();
  checkProductData(ctx);
}

function checkProductData(ctx) {
  ctx.checkBody('name').notEmpty();
  ctx.checkBody('price').notEmpty().isNumeric();
  ctx.checkErrors();
}
