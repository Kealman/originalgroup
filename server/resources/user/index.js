'use strict';

let controller = require('./user.controller');
let router = require('koa-router')();


router.get('/logout', controller.logout);
router.get('/:userId', controller.findOne);
router.post('/', controller.create);



module.exports = router.routes();
