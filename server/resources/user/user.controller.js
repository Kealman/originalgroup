'use strict';

let User = require('../../models/user');
let Storage = require('../../storage');


exports.findOne = function* (next) {
  if (!this.session.user || !Storage.getUser(this.session.user)) {
    this.status = 401;
    return;
  }

  this.body = Storage.getUser(this.session.user).getProfile();
};

exports.create = function* (next) {
  checkCreateUserData(this);

  let user = new User(this.request.body.name, this.request.body.bonus, this.request.body.bonusPercent);
  this.session.user = Storage.addUser(user);

  this.body = user.getProfile();
};

exports.logout = function* (next){
  this.session = null;
  this.status = 204;
};

function checkCreateUserData(ctx) {
  ctx.checkBody('name').notEmpty();
  ctx.checkBody('bonus').notEmpty().isNumeric();
  ctx.checkBody('bonusPercent').notEmpty().isNumeric();

  if (ctx.request.body.bonusPercent > 100) {
    ctx.errors.push({
      bonusPercent: 'Percent must be > 100'
    });
  }

  ctx.checkErrors();
}

