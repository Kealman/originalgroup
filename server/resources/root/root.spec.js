'use strict';


var app = require('../../server');
var request = require('supertest').agent(app.listen());

var expect = require('chai').expect;


describe('GET /api/v1', function(){
  it('should respond with 403', function(done){
    request
    .get('/api/v1')
    .expect(403, done);
  });
});
