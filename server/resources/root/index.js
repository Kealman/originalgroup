'use strict';

let controller = require('./root.controller');
let router = require('koa-router')();

router.get('/bower_components/*', controller.bowerComponents);
router.get('/app/*', controller.app);
router.get('*', controller.index);
module.exports = router.routes();
