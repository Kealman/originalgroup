'use strict';
let fs = require('fs');

let readFileThunk = function (src) {
  return new Promise(function (resolve, reject) {
    fs.readFile(src, {encoding: 'utf8'}, function (err, data) {
      if (err) {
        return reject(err);
      }
      resolve(data);
    });
  });
};

/**
 * Get index page
 */
exports.index = function *() {
  this.body = yield readFileThunk(`${__dirname}/../../../public/index.html`);
};

/**
 * Get bower components
 */
exports.bowerComponents = function *() {
  if (this.path.indexOf('.css') !== -1) {
    this.type = 'text/css';
  }
  this.body = yield readFileThunk(`${__dirname}/../../..${this.path}`);
};

/**
 * Get app files
 */
exports.app = function *() {
  if (this.path.indexOf('.css') !== -1) {
    this.type = 'text/css';
  }
  this.body = yield readFileThunk(`${__dirname}/../../../public${this.path}`);
};
