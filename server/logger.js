'use strict';

let config = require('./config'),
  PrettyStream = require('bunyan-prettystream'),
  bunyan = require('bunyan');

let env = config.util.getEnv('NODE_ENV');

let streams = [];

//Do not print pretty console in production
if (env !== 'production') {
  let prettyStdOut = new PrettyStream();
  prettyStdOut.pipe(process.stdout);
  streams.push({
    level: config.get('logger.level'),
    type: 'raw',
    stream: prettyStdOut
  });
}

//Create logger
module.exports = bunyan.createLogger({
  name: 'originalGroup',
  env: env,
  serializers: bunyan.stdSerializers,
  streams: streams
});
