'use strict';

let users = {};
let inc = 0;

exports.addUser = function(user) {
  users[++inc] = user;
  return inc;
};

exports.getUser = function(id) {
  return users[id];
};
