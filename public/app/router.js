'use strict';

bonusCalc.config(['$stateProvider', function ($stateProvider) {
  $stateProvider.state('main', {
    abstract: true,
    resolve: {
      profile: ['userService', function (userService) {
        return userService.getProfile();
      }]
    },
    template: '<div ui-view></div>'
  })
    .state('main.page', {
      url: '/',
      controller: 'productController',
      templateUrl: 'app/views/main.html'
    })
    .state('login', {
      url: '/login',
      controller: 'loginController',
      templateUrl: 'app/views/login.html'
    });
}]);
