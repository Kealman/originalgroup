'use strict';

var bonusCalc = angular.module('bonusCalc', ['ui.router', 'ngCookies']);

bonusCalc.config(['$locationProvider', '$httpProvider', function ($locationProvider, $httpProvider) {
  var interceptor = ['$injector', '$q', function ($injector, $q) {

    return {
      responseError: function (rejection) {
        if (rejection.status === 401) {
          $injector.get('userService').logout();
          $injector.get('$state').go('login');
          $('#errorContainer').text('Нужно созать пользователя').show();
        } else {
          $('#errorContainer').text('An unexpected error has occurred. Try again later').show();
        }
        return $q.reject(rejection);
      }
    };

  }];
  $httpProvider.interceptors.push(interceptor);

  $locationProvider.html5Mode(true);
}]).run(['$rootScope', '$state', function ($rootScope, $state) {

  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    if (error && (error.status === 401)) {
      if (toState && toState.name.indexOf('main') === 0) {
        $rootScope.afterLoginTo = {
          name: toState.name,
          params: toParams
        };
      } else {
        delete $rootScope.afterLoginTo;
      }
      $state.go('login');
    }

  });

  $rootScope.$on('$stateChangeStart', function (event, toState) {
    $('#errorContainer').hide();
  });
}]);

