'use strict';

angular.module('bonusCalc').factory('validateForm', [function () {
  return function (data, fieldScope) {
    var errors = [];

    data.forEach(function (val) {
      var condition = _.isFunction(val.condition) ? val.condition() : val.condition;

      if (!condition) {
        fieldScope[val.field] = true;
        errors.push(val.message);
      } else {
        fieldScope[val.field] = false;
      }
    });

    return errors;
  };
}]);
