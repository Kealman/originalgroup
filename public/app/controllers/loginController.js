'use strict';

function getUserValidateData(data) {
  return [
    {
      condition: !!data.name,
      field: 'name',
      message: 'Имя обязательно для заполнения'
    },
    {
      condition: typeof data.bonus === 'number',
      field: 'bonus',
      message: 'Бонус должен быть числом'
    },
    {
      condition: typeof data.bonusPercent === 'number' || data.bonusPercent < 100,
      field: 'bonusPercent',
      message: 'Процент возмещения должен быть числом и меньше 100'
    }
  ];
}

angular.module('bonusCalc').controller('loginController', ['$scope', 'userService', 'validateForm', '$state', '$cookies', function ($scope, userService, validateForm, $state, $cookies) {
  $scope.user = {};
  $scope.errors = [];
  $scope.errorFields = {};


  $cookies.remove('koa:sess');
  console.log($cookies.getAll());

  $scope.submit = function () {
    $scope.errors = validateForm(getUserValidateData($scope.user), $scope.errorFields);
    if (!$scope.errors.length) {
      userService.createUser($scope.user).then(function(){
        $state.go('main.page');
      });
    }
  };
}]);
