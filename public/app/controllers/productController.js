'use strict';

function getProductValidateData(data) {
  return [
    {
      condition: !!data.name,
      field: 'name',
      message: 'Имя обязательно для заполнения'
    },
    {
      condition: !!data.price && typeof data.price === 'number',
      field: 'price',
      message: 'Price должен быть числом'
    }
  ];
}

angular.module('bonusCalc').controller('productController', ['$scope', 'productService', '$rootScope', 'validateForm', function ($scope, productService, $rootScope, validateForm) {

  $scope.product = {};
  $scope.availableBonus = $rootScope.profile.bonus;
  $scope.products = $rootScope.profile.products;
  $scope.editProduct = {};
  $scope.editId = null;
  $scope.errors = [];
  $scope.errorFields = {};
  $scope.errorEditFields = {};


  function setScopeRequestData(data) {
    $rootScope.profile.products = data.productData;
    $rootScope.profile.bonus = data.currentAvailableBonus;
    $scope.availableBonus = data.currentAvailableBonus;
    $scope.products = data.productData;
  }

  $scope.addProduct = function (product) {
    $scope.errors = validateForm(getProductValidateData(product), $scope.errorFields);
    if($scope.errors.length){
      return;
    }
    productService.addProduct(product.name, product.price)
      .then(function (data) {
        setScopeRequestData(data);
      });
  };

  $scope.updateProduct = function (product) {
    $scope.errors = validateForm(getProductValidateData(product), $scope.errorEditFields);
    if($scope.errors.length){
      return;
    }
    productService.updateProduct(product.id, product.name, product.price)
      .then(function (data) {
        setScopeRequestData(data);
        $scope.cancelUpdate();
      });
  };

  $scope.deleteProduct = function(id) {
    productService.deleteProduct(id)
      .then(function (data) {
        setScopeRequestData(data);
      });
  };

  $scope.edit = function (prod) {
    $scope.editProduct = {
      id: prod.id,
      name: prod.name,
      price: prod.price
    };
    $scope.editId = prod.id;
  };



  $scope.cancelUpdate = function () {
    $scope.editId = null;
    $scope.editProduct = {};
  };

}]);
