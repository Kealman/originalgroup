'use strict';

angular.module('bonusCalc').service('userService', ['$http', '$rootScope', '$q', function ($http, $rootScope, $q) {
  return {
    getProfile: getProfile,
    createUser: createUser,
    logout: logout
  };

  function getProfile() {
    if ($rootScope.profile) {
      return $q(function (res) {
        res($rootScope.profile);
      });
    }
    return $http.get('/api/v1/user/profile')
      .then(function (data) {
        $rootScope.profile = data.data;
        return data.data;
      });
  }

  function createUser(data) {
    return $http.post('/api/v1/user', data);
  }

  function logout() {
    return $http.get('/api/v1/user/logout')
      .then(function () {
        delete $rootScope.profile;
      });
  }
}]);
