'use strict';

angular.module('bonusCalc').service('productService', ['$http', function ($http) {
  return {
    addProduct: addProduct,
    updateProduct: updateProduct,
    deleteProduct: deleteProduct
  };

  function addProduct(name, price) {
    return $http.post('/api/v1/product', {
      name: name,
      price: price
    }).then(function (res) {
      return res.data;
    });
  }

  function updateProduct(id, name, price) {
    return $http.put('/api/v1/product/' + id, {
      name: name,
      price: price
    }).then(function (res) {
      return res.data;
    });
  }

  function deleteProduct(id) {
    return $http.delete('/api/v1/product/' + id).then(function (res) {
      return res.data;
    });
  }
}]);
